﻿namespace Logistyka1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.myGrid = new System.Windows.Forms.DataGridView();
            this.labNumAction = new System.Windows.Forms.Label();
            this.btnAnalysis = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.labDuration = new System.Windows.Forms.Label();
            this.rtb = new System.Windows.Forms.RichTextBox();
            this.loadFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labCP = new System.Windows.Forms.Label();
            this.symbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.czas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.poprzednik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nastepnik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.myGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // myGrid
            // 
            this.myGrid.AllowUserToResizeColumns = false;
            this.myGrid.AllowUserToResizeRows = false;
            this.myGrid.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.myGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.myGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.myGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.symbol,
            this.czas,
            this.poprzednik,
            this.nastepnik});
            this.myGrid.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.myGrid.Location = new System.Drawing.Point(12, 49);
            this.myGrid.Name = "myGrid";
            this.myGrid.RowHeadersWidth = 30;
            this.myGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.myGrid.Size = new System.Drawing.Size(300, 523);
            this.myGrid.TabIndex = 0;
            // 
            // labNumAction
            // 
            this.labNumAction.AutoSize = true;
            this.labNumAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labNumAction.ForeColor = System.Drawing.Color.DarkBlue;
            this.labNumAction.Location = new System.Drawing.Point(324, 476);
            this.labNumAction.Name = "labNumAction";
            this.labNumAction.Size = new System.Drawing.Size(15, 24);
            this.labNumAction.TabIndex = 1;
            this.labNumAction.Text = " ";
            // 
            // btnAnalysis
            // 
            this.btnAnalysis.BackColor = System.Drawing.Color.SandyBrown;
            this.btnAnalysis.Location = new System.Drawing.Point(12, 585);
            this.btnAnalysis.Name = "btnAnalysis";
            this.btnAnalysis.Size = new System.Drawing.Size(121, 51);
            this.btnAnalysis.TabIndex = 2;
            this.btnAnalysis.Text = "Analizuj";
            this.btnAnalysis.UseVisualStyleBackColor = false;
            this.btnAnalysis.Click += new System.EventHandler(this.btnAnalysis_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tabela czynności";
            // 
            // labDuration
            // 
            this.labDuration.AutoSize = true;
            this.labDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.labDuration.ForeColor = System.Drawing.Color.DarkBlue;
            this.labDuration.Location = new System.Drawing.Point(324, 511);
            this.labDuration.Name = "labDuration";
            this.labDuration.Size = new System.Drawing.Size(15, 24);
            this.labDuration.TabIndex = 4;
            this.labDuration.Text = " ";
            // 
            // rtb
            // 
            this.rtb.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtb.Font = new System.Drawing.Font("Ubuntu Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtb.Location = new System.Drawing.Point(861, 467);
            this.rtb.Name = "rtb";
            this.rtb.ReadOnly = true;
            this.rtb.Size = new System.Drawing.Size(317, 169);
            this.rtb.TabIndex = 5;
            this.rtb.Text = "";
            // 
            // loadFile
            // 
            this.loadFile.Location = new System.Drawing.Point(148, 585);
            this.loadFile.Name = "loadFile";
            this.loadFile.Size = new System.Drawing.Size(121, 51);
            this.loadFile.TabIndex = 6;
            this.loadFile.Text = "załaduj plik";
            this.loadFile.UseVisualStyleBackColor = true;
            this.loadFile.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(328, 49);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(849, 402);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // labCP
            // 
            this.labCP.AutoSize = true;
            this.labCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.labCP.ForeColor = System.Drawing.Color.DarkBlue;
            this.labCP.Location = new System.Drawing.Point(324, 548);
            this.labCP.Name = "labCP";
            this.labCP.Size = new System.Drawing.Size(15, 24);
            this.labCP.TabIndex = 8;
            this.labCP.Text = " ";
            // 
            // symbol
            // 
            this.symbol.DataPropertyName = "symbol_prop";
            this.symbol.HeaderText = "Symbol";
            this.symbol.Name = "symbol";
            this.symbol.Width = 55;
            // 
            // czas
            // 
            this.czas.HeaderText = "Czas trwania";
            this.czas.Name = "czas";
            this.czas.Width = 55;
            // 
            // poprzednik
            // 
            this.poprzednik.HeaderText = "Zdarz. poprzedające";
            this.poprzednik.Name = "poprzednik";
            this.poprzednik.Width = 70;
            // 
            // nastepnik
            // 
            this.nastepnik.HeaderText = "Zdarz. następujące";
            this.nastepnik.Name = "nastepnik";
            this.nastepnik.Width = 70;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1190, 648);
            this.Controls.Add(this.labCP);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.loadFile);
            this.Controls.Add(this.rtb);
            this.Controls.Add(this.labDuration);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAnalysis);
            this.Controls.Add(this.labNumAction);
            this.Controls.Add(this.myGrid);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1198, 675);
            this.MinimumSize = new System.Drawing.Size(1198, 675);
            this.Name = "Form1";
            this.Text = "Logistyka w hutnictwie";
            ((System.ComponentModel.ISupportInitialize)(this.myGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView myGrid;
        private System.Windows.Forms.Label labNumAction;
        private System.Windows.Forms.Button btnAnalysis;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labDuration;
        internal System.Windows.Forms.RichTextBox rtb;
        private System.Windows.Forms.Button loadFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labCP;
        private System.Windows.Forms.DataGridViewTextBoxColumn symbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn czas;
        private System.Windows.Forms.DataGridViewTextBoxColumn poprzednik;
        private System.Windows.Forms.DataGridViewTextBoxColumn nastepnik;
    }
}

