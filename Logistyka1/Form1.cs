﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace Logistyka1
{
    public partial class Form1 : Form
    {

// klasa zdarzenie opisuje pojedyńcze zdarzenie, zgodnie z wymogami
        class Event
        {

            public double es;
            public double ef;
            public double ls;
            public double lf;
            public int prev, next;
            public double duration;
            public string symbol;
            public void set(string _symbol, double _duration, int _prev, int _next)
            {

                this.symbol = _symbol;
                this.duration = _duration;
                this.prev = _prev;
                this.next = _next;

            }

            public Event() {
                this.ls = 0;
            }

            public void print()
            {

                (Application.OpenForms["Form1"] as Form1).rtb.Text += //żeby dostać się do kontrolki w formie z innej klasy
                    "symbol zdarzenia:          " + this.symbol + "\n" +
                    "zdarzenie poprzedające:    " + this.prev + "\n" +
                    "zdarzenie następujące:     " + this.next + "\n" +
                    "czas trwania:              " + this.duration + "\n" +
                    "najpóźniejszy zakończenie: " + this.lf + "\n" + "\n"; 
            }
        }




        
        private System.Drawing.Graphics g;
       
        public Form1()
        {
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
            g.TranslateTransform(40, 40);
            //g.ScaleTransform(1, -1);
        }

        private void drawLayout(double _numHours, int _numRows)
        {


        }

        private void btnAnalysis_Click(object sender, EventArgs e)
        {
            this.rtb.Text = "";
            int numRows = myGrid.RowCount - 1;
            bool doAnalysis = true;
            double test = 0;

//tworzenie tablicy eventów            
            Event[] tabEvent = new Event[numRows+1];
            tabEvent[0] = new Event(); // w tej i następnej linijce, event zerowy jest ustawiany, jako next dostaje pierwszy event
            tabEvent[0].duration = 0; tabEvent[0].es = 0; tabEvent[0].ef = 0; tabEvent[0].ls = 0; tabEvent[0].lf = 0; tabEvent[0].next = 1;
            
//wypełnianie tablicy eventów danymi z grida
            int criticalEvent = 0;
            
            for (int i = 0; i <numRows; i++) {
                try
                {

                    tabEvent[i + 1] = new Event();
                    tabEvent[i + 1].symbol = myGrid.Rows[i].Cells[0].Value.ToString();
                    tabEvent[i + 1].duration = Convert.ToDouble(myGrid.Rows[i].Cells[1].Value);
                    tabEvent[i + 1].prev = Convert.ToInt16(myGrid.Rows[i].Cells[2].Value);
                    tabEvent[i + 1].next = Convert.ToInt16(myGrid.Rows[i].Cells[3].Value);
                    test = 1 / tabEvent[i + 1].duration;
                    test = 1 / tabEvent[i + 1].prev;
                    test = 1 / tabEvent[i + 1].next;
                }
                catch (System.NullReferenceException ex)
                {
                    rtb.Text += "BŁĄD: podaj symbol!";
                    doAnalysis = false;
                }
                catch (System.DivideByZeroException ex)
                {
                    rtb.Text += "BŁĄD: uzupełnij wsztstkie pola lub usuń puste wiersze!";
                    doAnalysis = false;
                }
                catch (System.FormatException ex) {
                    rtb.Text += "BŁĄD: uzupełnij wsztstkie pola lub usuń puste wiersze!";
                    doAnalysis = false;
                }
            }


            labDuration.Text = "";
            labCP.Text = "";
            labNumAction.Text = "";
            g.Clear(Color.White);

            if (doAnalysis && numRows != 0)
            {

                //ustalanie parametrów zdarzeń, czyli najpóźniejszego końca i początku

                for (int i = 1; i <= numRows; i++)
                {
                    for (int j = 0; j <= numRows; j++)
                    {
                        if (tabEvent[j].next == tabEvent[i].prev)
                        { //sprawdzam czy jest moim następnikiem
                            if (tabEvent[i].lf < tabEvent[j].lf + tabEvent[i].duration) //wybieram najdłuższy czas żeby było LS LF
                            {
                                tabEvent[i].ls = tabEvent[j].lf;
                                tabEvent[i].lf = tabEvent[i].ls + tabEvent[i].duration;
                            }
                        }
                        if (tabEvent[i].lf > tabEvent[criticalEvent].lf)
                        {
                            criticalEvent = i;
                        }
                    }
                    tabEvent[i].print();
                }



                labNumAction.Text = "Liczba czynności: " + numRows.ToString();
                labDuration.Text = "Całkowity czas trwania: " + tabEvent[criticalEvent].lf;

                // poszukiwanie ścieżki krytycznej

                int[] criticalPath = new int[numRows + 1];
                int tempCE = criticalEvent;
                criticalPath[0] = criticalEvent;
                for (int i = 1; i <= numRows + 1; i++)
                    for (int j = 0; j < numRows; j++)
                    {
                        if (tabEvent[j].next == tabEvent[tempCE].prev && tabEvent[j].lf == tabEvent[tempCE].ls) //sprawdzam czy obecny jest następnikiem i czy kończy się wtedy co ja zaczynam
                        {
                            tempCE = j;
                            criticalPath[i] = j;
                        }

                    }

                labCP.Text = "Ścieżka krytyczna: ";

                for (int k = criticalPath.Length -1; k >= 0; k--)
                {
                    if (criticalPath[k] > 0)
                    labCP.Text += tabEvent[criticalPath[k]].symbol + "  ";
                    //if (criticalPath[k] >= 0)
                        //labCP.Text +=  tabEvent[criticalPath[k]].symbol +" ";
                }

                // rysowanie układu 

                g.Clear(Color.White);
                System.Drawing.Pen boldPen = new System.Drawing.Pen(Color.Black, 2);
                System.Drawing.Pen thinPen = new System.Drawing.Pen(Color.Gray, 1);
                System.Drawing.Pen bluePen = new System.Drawing.Pen(Color.Blue, 8);
                System.Drawing.Pen redPen = new System.Drawing.Pen(Color.Red, 12);

                g.DrawLine(boldPen, 0, -20, 0, 320); //y
                g.DrawLine(boldPen, -20, 0, 760, 0); //x
                System.Drawing.Font drawFont = new System.Drawing.Font("Arial", 12);
                System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);


                // jednostka skalowalna unit
                int yUnit, xUnit;
                double numHours = tabEvent[criticalEvent].lf;
                if (numRows > 0 && numHours > 0)
                {
                    yUnit = Convert.ToInt16(320 / numRows);
                    xUnit = Convert.ToInt16(760 / numHours);

                    for (int i = 0; i <= numRows; i++)
                    {
                        g.DrawLine(boldPen, -10, (yUnit * i), 10, (yUnit * i));
                        if (i > 0)
                            g.DrawString(tabEvent[i].symbol, drawFont, drawBrush, -30, yUnit * i - 10);
                    }
                    for (int i = 0; i <= numHours; i++)
                    {
                        g.DrawLine(thinPen, (xUnit * i), -10, (xUnit * i), 350);
                        g.DrawLine(boldPen, (xUnit * i), -10, (xUnit * i), 10);
                        if (i > 0)
                            g.DrawString("" + i, drawFont, drawBrush, xUnit * i - 10, -30);
                    }

                    // rysowanie czynności

                    for (int i = 1; i <= numRows; i++)
                    {
                        int beg, end;

                        beg = Convert.ToInt32(xUnit * tabEvent[i].ls);
                        end = Convert.ToInt32(xUnit * tabEvent[i].lf);

                        for (int j = 0; j < criticalPath.Length; j++)
                        {
                            if (i == criticalPath[j]) // jeżeli czynność jest krytyczna rysowana jest na czerwono
                            {
                                g.DrawLine(redPen, beg, yUnit * i, end, yUnit * i);
                                break; // żeby nie nadrysowywał wywala z pętli
                            }
                            else
                            {
                                g.DrawLine(bluePen, beg, yUnit * i, end, yUnit * i);
                            }
                        }
                    }
                }
            }
        }
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Displays an OpenFileDialog so the user can select a Cursor.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Cursor Files|*.txt";
            openFileDialog1.Title = "otwórz plik z danymi";

            // Show the Dialog.
            // If the user clicked OK in the dialog and
            // a .CUR file was selected, open it.
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                myGrid.Rows.Clear();
                myGrid.Refresh();
                System.IO.StreamReader sr = new
                System.IO.StreamReader(openFileDialog1.FileName);
                //rtb.Text = sr.ReadToEnd();
                string line;
                int n = 0;
                line = sr.ReadLine();
                while ((line = sr.ReadLine()) != null)
                {
                    rtb.Text += line + "\n";
                    string[] split = line.Split('\t');
                    string test = "";
                    double testS = 0;
                    myGrid.Rows.Add();

                        myGrid.Rows[n].Cells[0].Value = split[0];
                        myGrid.Rows[n].Cells[1].Value = split[1];
                        myGrid.Rows[n].Cells[2].Value = split[2];
                        myGrid.Rows[n].Cells[3].Value = split[3];

                        n++;
                }
                
                //MessageBox.Show(sr.ReadToEnd());
                sr.Close();
            }
        }


    }
}
